import React from "react";

import CountryInfo from "./containers/CountryInfo/CountryInfo";

import './App.css';

const App = () => {
  return (
      <div className="App">
        <CountryInfo />
      </div>
  );
}

export default App;