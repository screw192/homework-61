import React, {useState, useEffect} from 'react';
import axios from "axios";

import CountryListItem from "../../components/CountryListItem/CountryListItem";
import CountryDetails from "../../components/CountryDetails/CountryDetails";
import "./CountryInfo.css";

const ALL_COUNTIES_URL = "all?fields=name;alpha3Code";

const CountryInfo = () => {
  const [countryData, setCountryData] = useState([]);
  const [selectedAlpha3, setSelectedAlpha3] = useState(null);

  useEffect(() => {
    axios
        .get(ALL_COUNTIES_URL)
        .then(response => {
          setCountryData(response.data);
        });
  }, []);

  const countryList = countryData.map(country => {
    return (
        <CountryListItem
            key={country["alpha3Code"]}
            id={country["alpha3Code"]}
            name={country["name"]}
            click={setSelectedAlpha3}
        />
    );
  });

  return (
      <>
        <div className="CountryList">
          {countryList}
        </div>
        <div className="DetailedInfo">
          <CountryDetails id={selectedAlpha3}/>
        </div>
      </>
  );
};

export default CountryInfo;