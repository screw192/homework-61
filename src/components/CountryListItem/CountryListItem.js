import React, {PureComponent} from 'react';
import "./CountryListItem.css";

class CountryListItem extends PureComponent {
  render() {
    return (
        <button
            id={this.props.id}
            className="CountryListItem"
            onClick={() => this.props.click(this.props.id)}
        >{this.props.name}</button>
    );
  }
}

export default CountryListItem;