import React, {useEffect, useState, useCallback} from 'react';
import axios from "axios";

import "./CountryDetails.css";

const CountryDetails = ({id}) => {
  const [selectedCountry, setSelectedCountry] = useState(null);
  const [selectedCountryBorders, setSelectedCountryBorders]  = useState([]);

  const fetchData = useCallback(async () => {
    if (id !== null) {
      const countryResponse = await axios.get("alpha/" + id);

      setSelectedCountry(countryResponse.data);

      if (countryResponse.data["borders"].length !== 0) {
        const bordersAlphaBlock = countryResponse.data["borders"].join(';');
        const borderResponse = await axios.get("alpha?codes=" + bordersAlphaBlock);

        setSelectedCountryBorders(borderResponse.data);
      } else {
        setSelectedCountryBorders([]);
      }
    }
  }, [id]);

  useEffect(() => {
    fetchData().catch(console.error);
  }, [id, fetchData]);

  let countryBordersList;

  if (selectedCountryBorders.length !== 0) {
    countryBordersList = (
        <>
          <h4>Borders with:</h4>
          <ul className="BordersList">
            {selectedCountryBorders.map(country => {
              return (
                  <li key={country["alpha3Code"]}>{country["name"]}</li>
              );
            })}
          </ul>
        </>
    );
  } else {
    countryBordersList = (
        <h4>Borders with no countries</h4>
    );
  }

  return selectedCountry && (
      <>
        <div className="DetailsMainBlock">
          <div className="MainDetails">
            <h3 className="CountryName">{selectedCountry["name"]}</h3>
            <p className="AdditionalInfo"><b>Region:</b> {selectedCountry["region"]} / {selectedCountry["subregion"]}</p>
            <p className="AdditionalInfo"><b>Capital:</b> {selectedCountry["capital"]}</p>
            <p className="AdditionalInfoArea"><b>Area:</b> {selectedCountry["area"]} km<sup>2</sup></p>
            <p className="AdditionalInfo"><b>Population:</b> {selectedCountry["population"]}</p>
          </div>
          <img
              className="CountryFlag" src={selectedCountry["flag"]} alt={selectedCountry["name"]}
              height="100px" width="auto"/>
        </div>
        <div className="BordersInfo">
          {countryBordersList}
        </div>
      </>
  );
};

export default CountryDetails;